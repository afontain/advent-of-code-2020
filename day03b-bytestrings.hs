import qualified Data.ByteString.Lazy as B
import Control.Monad.State

type Address = Int

width = 31
height = 322

treeAt :: Address -> State (Address, B.ByteString) Int
treeAt addr =
     do (currAddr, bs) <- get
        let bs' = B.drop (fromIntegral $ addr - currAddr) bs
        put (addr, bs')
        return (isTree $ B.head bs')
    where isTree 35 = 1 -- 35 is '#'
          isTree _ = 0

treesEncountered :: B.ByteString -> (Int, Int) -> Int
treesEncountered trees (slopeX, slopeY) =
    let coordsToCheck = [(t*slopeX, t*slopeY) | t<-[1..height`div`slopeY]]
        indices = [y*(width+1) + x`mod`(width+1) | (x, y)<-coordsToCheck]
        count = sum <$> sequence (map treeAt indices)
    in fst $ runState count (0, trees)

main = do
        trees <- B.readFile "day03-input"
        let slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
        print $ product $ map (treesEncountered trees) slopes
