import Data.List (dropWhileEnd)
import Data.Char (ord)
import qualified Data.Map as Map
import Debug.Trace (traceId)

data Password = Password Int Int Char String

countChars :: String -> Map.Map Char Int
countChars str = foldr count initial str
	where initial :: Map.Map Char Int
	      initial = Map.fromList [(c, 0::Int) | c<-['a'..'z']]
	      count c = Map.insertWith (+) c 1

checkPassword :: Password -> Bool
checkPassword (Password min max c str) =
    none (\k x -> k==c && x >= min && x <= max)
	where none predicate =
		Map.foldrWithKey (\k x acc -> predicate k x || acc)
	                         False passwds
	      passwds = countChars str

countCorrect :: [Password] -> Int
countCorrect input = sum $ map (toInt . checkPassword) input
	where toInt True = 1
	      toInt False = 0

parse :: String -> [Password]
parse str = map readLn (lines str)
	where readLn line = readLn' (words line)
	      readLn' [bounds, chars, passwd] =
		Password min max (head chars) passwd
			where min = read $ takeWhile (/= '-') bounds
			      max = read $ tail $ dropWhile (/= '-') bounds

main = readFile "day2-input" >>= print . countCorrect . parse
