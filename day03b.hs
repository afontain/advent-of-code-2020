type Tree = Int -- Eww…

width = 31
height = 322

parse :: String -> [[Tree]]
parse str = map parseLine $ lines str
    where parseLine = map isTree
          isTree '#' = 1
          isTree '.' = 0
          isTree _ = error "wtf"

treesEncountered trees (slopeX, slopeY) =
        let treeAt (x, y) = let x' = x `mod` width in trees !! y !! x'
            coordsToCheck = [(t*slopeX, t*slopeY)
                                | t<-[1..height`div`slopeY]]
        in sum $ map treeAt coordsToCheck

main = do
        trees <- parse <$> readFile "day03-input"
        let slopes = [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
        print $ product $ map (treesEncountered trees) slopes
