import Data.List (dropWhileEnd)
import Debug.Trace (traceShowId)

x `xor` y = (x || y) && (not (x && y))

data Password = Password Int Int Char String deriving Show

checkPassword :: Password -> Bool
checkPassword (Password first second c str) =
    (str!!(first-1) /= c) `xor` (str!!(second-1) /= c)

countCorrect :: [Password] -> Int
countCorrect input = sum $ map (toInt . checkPassword) input
	where toInt True = 1
	      toInt False = 0

parse :: String -> [Password]
parse str = map readLn (lines str)
	where readLn line = readLn' (words line)
	      readLn' [bounds, chars, passwd] =
		Password min max (head chars) passwd
			where min = read $ takeWhile (/= '-') bounds
			      max = read $ tail $ dropWhile (/= '-') bounds

main = readFile "day02-input" >>= print . countCorrect . parse
