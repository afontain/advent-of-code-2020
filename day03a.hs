
type Tree = Int -- Eww…

width = 31
height = 322

parse :: String -> [[Tree]]
parse str = map parseLine $ lines str
    where parseLine = map isTree
          isTree '#' = 1
          isTree '.' = 0
          isTree _ = error "wtf"

main = do
	trees <- parse <$> readFile "day03-input"
	let coordsToCheck = [(t*3, t*1) | t<-[1..height]]
	let treeAt (x, y) = let x' = x `mod` width in trees !! y !! x'
	print $ sum $ map treeAt coordsToCheck
